// Sample webhook showing what a hasura auth webhook looks like

// init project
var express = require('express');
var app = express();
var requestClient = require('request');
var port = process.env.PORT || 3000;
var jwt = require('jsonwebtoken');
require('es6-promise').polyfill();
require('isomorphic-fetch');

/* A simple sample
   Flow:
   1) Extracts token 
   2) Fetches userInfo in a mock function
   3) Return hasura variables
*/
function fetchUserInfo (token, cb) {
  // This function takes a token and then makes an async
  // call to the session-cache or database to fetch
  // data that is needed for Hasura's access control rules
  cb();
}
app.get('/', (req, res) => {
  res.send('Webhooks are running');
});

app.get('/simple/webhook', (request, response) => {

  const currentDate = () => {
    const today = new Date();
    const dd = today.getDate() < 10 ? "0" + today.getDate() : today.getDate();
    const mm =
      today.getMonth() + 1 < 10
        ? "0" + (today.getMonth() + 1).toString()
        : (today.getMonth() + 1).toString();
    const yyyy = today.getFullYear();

    return yyyy + "-" + mm + "-" + dd;
  }

   // Extract token from request
  var token = request.get('Authorization');
  var userId;
  if (token !== undefined) {
    var TokenArray = token.split(" ");
    var decoded = jwt.decode(TokenArray[1].toString())
    userId = decoded.unique_name.substr(1, 7);
  }

  var query = `query($currentDate: date, $soldierId: Int){
    users_users(where:{soldier_id:{_eq:$soldierId}}) {
      instructors(where:{department:{cycle:{start_date:{_lt:$currentDate},end_date:{_gt:$currentDate}}}}){
        department{
          cycle{
            start_date
            end_date
          }
        }
      }
      students(where:{department:{cycle:{start_date:{_lt:$currentDate},end_date:{_gt:$currentDate}}}}){
        department{
          cycle{
            start_date
            end_date
          }
        }
      }
    }
  }`;

  fetch('http://ocean.it.bsmch.net/v1alpha1/graphql', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'X-Hasura-Access-Key': 'developmenteam'
    },
    body: JSON.stringify({
    query,
    variables: { currentDate: currentDate(),
      soldierId: userId }
  })
})
  .then(r => r.json())
  .then(data => successFunction(data))

  const successFunction = (data) => {

    const role = data.data.users_users[0].instructors.length !== 0 ? 'instructor' :  data.data.users_users[0].students.length !== 0 ? 'student' : '';

    // Fetch user_id that is associated with this token
    fetchUserInfo(token, (result) => {
  
      // Return appropriate response to Hasura
      var hasuraVariables = {
        'X-HASURA-ROLE': role,  // result.role
        'X-HASURA-USER-ID': userId    // result.user_id
      };
      response.json(hasuraVariables);
    });
  }
});


// listen for requests :)
var listener = app.listen(port, function () {
  console.log('Your app is listening on port ' + port);
});
